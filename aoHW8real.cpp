/*This program reads from student.txt*/
#include <iostream> //Importing libraries
#include <fstream>
#include <string>

using namespace std; //Using namespace

int main()
{
	string line, filename = "student.txt"; //Defining filename and telling the program what to look for
	char ch; //Defining ch
	ifstream inFile;
	ofstream outFile;
	
	inFile.open(filename.c_str());
	if (inFile.good()) //Checking to see if file exists
	{
		cout<<filename<<" already exists -overwrite (y/n) ? "; //Giving the user the option to over write the file
		cin>>ch;
		if (ch=='n') //If the user puts in n, the program is ended.
		{
				inFile.close();
				cout<<"\n\nEnter 'e' to exit... ";
				cin>>ch;
				return 0;
		}
	}
	inFile.close(); //Closing the file
	
	return 0; //End of program
}

