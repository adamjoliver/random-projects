/*This program converts polar vectors to rectangular vectors */

#include <iostream> //Importing libraries
#include <cmath>
#include <math.h>
#define _USE_MATH_DEFINES

using namespace std; //Using namespace

void polar_to_rect(double,double,double &,double &);

int main()//Initializing
{
	double mag, ang, x, y;
	
	char ch;

	while (ch != 'n')
	{
	
		cout<<"This is a program to convert polar vectors into rectangular vectors.";
		cout<<"\nPlease provide the magnitude of the polar vector: ";
		cin>>mag;
		cout<<"Please provide the angle of the polar vector: ";
		cin>>ang;
		ang=ang*(M_PI / 180);
		polar_to_rect(mag,ang,x,y);
	
	
		cout<<"X equals: "<<x;
		cout<<"\nY equals: "<<y;
		cout<<"\nWould you like to do another calculation? Press y if yes, or n for no. ";
		cin>>ch;
	}
	return 0;
}
	
void polar_to_rect(double _mag, double _ang,double &x,double &y)
 {
	  x= _mag*cos(_ang);
	  y= _mag*sin(_ang);
 } 
