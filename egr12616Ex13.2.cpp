/*
    W. Rodney Owen
    EGR 126 - Chapter 13 - Linked List
    Example 13.2 - 4/16/15
    
    A linked list uses a pointer to indicate the next "struct" in a chain creating
    a list in which "structs" can be added or removed.
*/
#include<iostream>                                         //Libraries needed
#include<string>
#include<iomanip>
using namespace std;                                       //Library location

struct PartID                                              //"struct" declared
{                                                          //All data members are public here                                                          
    int partnum;
    string partname;
    double partcost;
    PartID *nextaddr;                                      //Pointer to next address
}; 

int main()
{
    cout<<"                        ***Example 13.2 - Linked List***"<<endl<<endl;
    
    char ch; 
    int i; 
    PartID p1 = {1234, "Washer", 0.56};
    PartID p2 = {4553, "Bolt", 1.09};
    PartID p3 = {8976, "Screw", 0.78, &p2};
    PartID *first;
    
    first = &p3;
    //p3.nextaddr = &p2;                                    //Alternate to initialization
    p2.nextaddr = &p1;
    p1.nextaddr = NULL;
    
    //Display the parts database
    i=0;
    cout<<"Record #  "<<setw(10)<<"Part Num|"<<setw(11)<<" Part Name|"<<setw(11)<<" Part Cost|"<<endl;
    cout<<"--------------------------------------------------------------------------"<<endl;
    cout<<"Record "<<i<<":"<<setw(11)<<first->partnum<<setw(11)<<first->partname<<setw(7)<<"$"<<first->partcost<<endl;
    i++;
    cout<<"Record "<<i<<":"<<setw(11)<<p3.nextaddr->partnum<<setw(11)<<p3.nextaddr->partname<<setw(7)<<"$"<<p3.nextaddr->partcost<<endl;
    i++;
    cout<<"Record "<<i<<":"<<setw(11)<<p2.nextaddr->partnum<<setw(11)<<p2.nextaddr->partname<<setw(7)<<"$"<<p2.nextaddr->partcost<<endl;
    
   
    cout<<"\n\nEnter 'e' to exit... ";                     //Hold screen for viewing
    cin>>ch;
    return 0;                                              //End of program.
}             
