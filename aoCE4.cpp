/*This program writes my name to a text file*/
#include <iostream> //Importing libraries
#include <fstream>
#include <string>

using namespace std; //Using namespace

int main()
{
	string line, filename = "my_name.txt";
	char ch;
	ifstream inFile;
	ofstream outFile;
	
	inFile.open(filename.c_str());
	if (inFile.good())
	{
		cout<<filename<<" already exists -overwrite (y/n) ? ";
		cin>>ch;
		if (ch=='n')
		{
				inFile.close();
				cout<<"\n\nEnter 'e' to exit... ";
				cin>>ch;
				return 0;
		}
	}
	inFile.close();
	
	outFile.open(filename.c_str());
	
	if (outFile.fail())
	{
		cout<<filename<<" did not open successfully for writing."<<endl;
		cout<<"\n\nEnter 'e' to exit... ";
		cin>>ch;
		return 0;
	}
	
	cout<<endl<<endl<<filename<<" is open for writing:"<<endl;
	
	outFile<<"Adam J. Oliver"<<endl;
	
	outFile.close();
	return 0;
}

	
	
	
	
	
	
	
	
	
	
	
	
	
	

