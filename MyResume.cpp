/*Hey, author of the program/resume, Adam, here. I tried to make the code as clean and readable as possible, but since I'm still learning there may be extra tricks I may not know of 
to make it even cleaner! Please feel free to provide input, be it on the code or resume, even if you do not hire me! Thanks!*/
#include <iostream> //Importing libraries
#include <fstream>
#include <string>

using namespace std; //Using namespace

int main()
{

	int x;
	{
 	
		 { //Moving most of the text here to make things look tidier	 
		 
		 cout<<"Hello! You are now looking at my resume, written in C++!"; //Some intro
		 cout<<"\nHere are the available options:"; //Commencing option index
		 locA: //Hacky solution to keep the user from having to constantly restarting the program
		 cout<<"\nPlease input '1' to see things like my contact information and location";
		 cout<<"\nPlease input '2' if you would like to see the list of operating systems I have experience with.";
		 cout<<"\nPlease input '3' if you would like to see my interests.";
		 cout<<"\nPlease input '4' to see the list of software I have knowledge of";
		 cout<<"\nPlease input '5' to see my experience in the field of computer software and hardware";
		 cout<<"\nPlease input '6' to obtain a link to my resume on OneDrive";
		 cout<<"\nPlease input '7' to exit the program"<<endl;
		 cin>>x;
		 /* Commence the if else statements! I know, I could have used switches, but I wanted to catch any invalid keyboard input. Woo forward thinking. 
		 That is, if I can get my error catching working...*/
		  
			
			 if (x==1) //If the user puts in "1", print the following information 
			  {
			    cout<<"My personal information is as follows...";
			    cout<<"\nMy full name is Adam Joshua Oliver";
			    cout<<"\nMy email is adamjoliver@live.com";
			    cout<<"\nMy phone number is 540-589-6098";
			    cout<<"\nMy physical address is 1331 Forest Lawn Drive, 24153 in Salem, VA"<<endl;
			    goto locA; //This forces the program to loop back to the instruction list, instead of exiting the program. 
			  }
			 else if (x==2) //If the user inputs "2", print the following information
			  {
			    cout<<"I have experience with the following operating systems...";
			    cout<<"\nWindows XP";
			    cout<<"\nWindows ME";
			    cout<<"\nWindows Vista";
			    cout<<"\nWindows 7";
			    cout<<"\nWindows 8/8.1";
			    cout<<"\nWindows 10";
			    cout<<"\nMost Debian based distributions, including the Ubuntu family and SteamOS";
			    cout<<"\nArch Linux based distributions"<<endl;
			    goto locA; //Forcing the program to go back to the list
			  }
			 else if (x==3) //If the user inputs "3", print the following information
			  {
			  	cout<<"The following are my interests";
			  	cout<<"\nComputer building";
			  	cout<<"\nComputer hardware, CPU and GPU architectures in particular";
			  	cout<<"\nExperimentation with Linux distributions"<<endl;
			  	goto locA; //Forcing the program to go back to the list
			  }
			 else if(x==4) //If the user inputs "4", print the following information
			  {
			  	cout<<"The following are the pieces of software I have used and am experienced with";
			  	cout<<"\nGPU driver installation, including Nvidia, AMD, and Intel";
			  	cout<<"\nWindows Disk Management and Gparted for linux distributions";
			  	cout<<"\nThe whole Microsoft Office Suite";
			  	cout<<"\nBenchmarking and stress testing tools such as MSI Kombustor, AIDA64, Prime95";
			  	cout<<"\nThe Safari, Chrome/Chromium, Edge, Internet Explorer, and Firefox web browsers";
			  	cout<<"\nBIOS Flashing";
			  	cout<<"\nTruecrypt and Veracrypt";
			 	cout<<"\nThe Steam game client";
				cout<<"\nOBS, short for Open Broadcasting Software";
				cout<<"\nPlex Media Server";
				cout<<"\nMalware cleanup programs such as Malwarebytes, Adwcleaner, CCleaner";
				cout<<"\nAntiviruses such as Avast, Norton, McAfee, Avira, and Kaspersky"<<endl;
				goto locA; //Forcing the program to go back to the list	
			   }
			 else if(x==5) //If the user inputs "5", print the following information
			  {
			   cout<<"\nI have built two computers(pictures and specs available upon request)";
			   cout<<"\nI have served as server administrator of several sizeable modded minecraft servers, being in charge of lag fixing, mod additions and removal through FTP, and crash diagnosing";
			   cout<<"\nI have provided tech support to friends and acquaintances on several occasions";
			   cout<<"\nI have performed reinstalls on several Windows machines";
			   cout<<"\nI have knowledge of C++";
			   cout<<"\nI have served as beta tester for two major minecraft modpacks, Resonant Rise and Test Pack Please Ignore"<<endl;
			   goto locA; //Forcing the program to go back to the list
			  }
			 else if(x==6)//If the user inputs 6, output the link to my online resume
			  {
			   cout<<"https://onedrive.live.com/redir?resid=7AB7BF235AF2EB1D!1854&authkey=!AM3tQn0QUpet53k&ithint=file%2cdocx"<<endl;
			  }
	
		 
		  
		  
		  
		 
		  
	
	}

 }
 return 0;
}
