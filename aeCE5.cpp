#include <iostream> //Preprocessor commands
#include <string>
using namespace std; //Using namespace library

int main() //Driver function header
{
	int num=1234; //initializing and defining the variable "num"
	int *ptr; //initializing pointer
	int my_array[]={1,2,3,4,5}; //initializing and defining array
	ptr=my_array; //assining ptr to array
	cout<<my_array<<endl;
	cout<<my_array[4]<<endl;
	cout<<ptr<<endl;
	cout<<*(ptr+4)<<endl;
	cout<<*++ptr<<endl;
	cout<<ptr<<endl;
	cout<<*ptr--<<endl;
	cout<<ptr<<endl;
	cout<<*my_array<<endl;
	
	
	
	
	return 0;
}
