//Importing libraries

#include <iostream>
#include <cmath>

using namespace std;

int main()  //Initializing...
{
double k, T_h, T_i, T_f, T_a, T_d; //Defining my values as doubles

k = (-log((T_f - T_a)/(T_i - T_a)))/1.0; //My name is k. I am an equation.

T_d = 98.6; //Individual's body temperature at death. This is in Fahrenheit

T_h = (-log((T_i - T_a)/(T_d - T_a)))/k; //Determing amount of time between death of individual and arrival to scene

cout <<"Newton's Law of Cooling says that the rate of change of the temperature of an object is proportional to the difference in temperature between the object and the ambient environment or dT(t)/dt = -k (T - T_a) where k is a constant." << endl <<endl << "The problem: The medical examiner arrives at the crime scene and takes the temperature, T_i, of the deceased. An hour later the medical examiner again takes the temperature T_f of the deceased. The examiner notices that the thermostat in the room is set for a constant T_a temperature. " <<endl <<endl ;

cout <<"Please input temperature of individual at time of arrival to scene: "; //Giving the user some instructions

cin >> T_i; //User inputs data for T_i

cout << "Please input temperature of individual one hour later: ";

cin >> T_f; //Now for T_f

cout << "Please input temperature of room: ";

cin >> T_a; //And finally, T_a

cout << "The amount of time in hours between death of the individual and the examiner's arrival: "<< T_h;	
}
