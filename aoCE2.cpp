/*
  Adam Oliver
  EGR 126 - Class Exercise - CE2
  
  Write and debug a program that asks the user to enter a character (declare a character variable called ch).  
  Then, check the variable to see if it is one of the characters 'a', 'b', or 'c' - display the character.  If not one of the three characters, 
  inform the user that 'a', 'b', or 'c' were not entered.
*/

	#include <iostream> //Importing library
	
	using namespace std; //Using namespace
	
	int main() //Initializing...
{
	char ch; //defining the variable ch as a character
	
	cout<<"Please enter either a, b, or c "; //Telling the user what to do
	cin>>ch;  //Inputting user character
	switch (ch)
	{
	
	case 'a':
		cout<<"You put in a.";
		break;
	case 'b':
		cout<<"You put in b.";
		break;
	case 'c':
		cout<<"You put in c.";
		break;
	
	default:
	cout<<" You put in an invalid character. Please follow my directions next time. :)";
}
system("Pause")	
return 0;

}

