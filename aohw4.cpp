/* This is a program to solve for the variable X in a*x^2+b*x+c=0. */

#include <iostream> //importing libraries
#include <cmath>
#include <math.h>

using namespace std; //Using namespace

int main() //Initializing...
{
	double a, b, c, x, x1, x2, i1, i2; //Defining variables as doubles
	
	cout<<"This is a program designed to solve for the variable X in a*x^2+b*x+c=0";
	
	cout<<"\nPlease input a value for a: ";
	cin>>a;
	
	cout<<"Please input a value for b: ";
	cin>>b;
	
	cout<<"Please input a value for c: ";
	cin>>c;

	
	if (a==0 and b==0) //If a and b both equal 0, then X can not be determined
	 {
	 cout<<"The value for X could not be determined.";}
	
	else(a==0 and b!=0); //However, if b does not equal 0, then X is the quotient of -c/b	 x==-c/b;
	 {
	 cout<<"The value for X is "<<x;}
	 
	if(a!=0 and (pow(b,2.0)-4*a*c)==0)  //If a does not equal 0 and b^2-4*a*c does equal Zero, then X is the quotient of -b/(2*a)
	{
	x==-b/(2*a);
	cout<<"The value for X is "<<x;}

	else (a!=0 and (pow(b,2.0)-4*a*c)>0); //If a does not equal 0 and b^2-4*a*c is greater then 0, there are two possible X values
	 {
	 x1==(-b/(2*a)+(sqrt(4*a*c-pow(b,2))/(2*a)));
	 x2==(-b - sqrt(pow(b,2) - 4*a*c))/(2*a);
	 cout<<"There are two possible values for X. The first one is "<<x1;
	 cout<<"/nThe second possible value for X is "<<x2;}
	 


	if (a!=0 and (pow(b,2)-4*a*c)<0) //If a does not equal 0 and b^2-4*a*c is greater then 0, there are two possible x values
	 {
	 i1==sqrt(4*a*c-pow(b, 2))/(2*a);
	 i2==-(sqrt(4*a*c - pow(b,2))/(2*a));
	 x1==(-b/(2*a)+i1);
	 x2==(-b/(2*a)+i2);
	 cout<<"There were two possible values for X. The first one is "<<x1;
	 cout<<"/nThe second possible value for X is "<<x2;}
	 else 
	 {
	 cout<<"There was an error. Please contact the writer of the program. :(";}

system("Pause");
return 0;	
}

