#include <iostream>
#include <cmath> //Telling the program what libraries we're using

using namespace std;

class Two_nums
{
	private:
		double num1;
		double num2;	//Declaring input functions
	public:
		//Two_nums(double,double)
		double add();	
		double avg();
		void chg_nums(double,double);
		void get_nums(double &, double &);	//Declaring output functions	
};
double Two_nums::add() //Defining the function "add"
{
	return(num1+num2);
}
double Two_nums::avg()	//Defining the function "avg"
{
	return((num1+num2)/2);
}
void Two_nums::chg_nums(double a, double b)
{
	a = num1;
	b = num2;
	
	return; //Defining the function "chg_nums"
}
void Two_nums::get_nums(double &a, double &b)
{
	a = num1;
	b = num2;
	return; //Defining the function "get_nums"
}
//End of class

int main() //Initializing main program
{
	double num1, num2; //Declaring num1 and num2
	Two_nums two1, two2; //Instantiating objects
	two1.chg_nums(10.5, 20.5); //Changing numbers
	two1.get_nums(num1,num2); //Getting numbers
	cout<<"Current value for num1 "<<num1<<endl; //Outputting first round of calculations
	cout<<"Current value for num2 "<<num2<<endl;
	cout<<"Adding together num1 and num2 "<<two1.add()<<endl;
	cout<<"Averaging together num1 and num2"<<two1.avg()<<endl;
	two2.chg_nums(3.5,9.9); //Changing num1 and num2
	cout<<"Current value for num1 "<<num1<<endl; //Outputting second round of calculations
	cout<<"Current value for num2 "<<num2<<endl;
	cout<<"Adding together num1 and num2 "<<two2.add()<<endl;
	cout<<"Averaging together num1 and num2 "<<two2.avg()<<endl;
	return 0;
}
